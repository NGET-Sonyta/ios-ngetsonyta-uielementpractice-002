//
//  ViewController.swift
//  iOS_UIElement_Practice
//
//  Created by Nyta on 11/13/20.
//

import UIKit

protocol MessageDelegate{
    func didSend(userName: String, email: String, address: String, phoneNum: String)
    
    func showImg(img: UIImage, color: UIColor)
}


class ViewController: UIViewController {

    @IBOutlet weak var firstImg: UIImageView!
    @IBOutlet weak var txtPhoneNum: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtUsername: UITextField!
    
    var delegate: MessageDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtUsername.becomeFirstResponder()
        txtEmail.becomeFirstResponder()
        txtAddress.becomeFirstResponder()
        txtUsername.becomeFirstResponder()
        
        txtEmail.keyboardType = .emailAddress
        txtPhoneNum.keyboardType = .phonePad
        
        txtUsername.delegate = self
        txtEmail.delegate = self
        txtAddress.delegate = self
        txtPhoneNum.delegate = self
    }

    @IBAction func btnSend(_ sender: Any) {
        let image = UIImage(systemName: "person.fill")
        let imageColor = firstImg.tintColor
        
        let userName = txtUsername.text!
        let address = txtAddress.text!
        let email = txtEmail.text!
        let phoneNum = txtPhoneNum.text!
        let redPlaceholder = NSAttributedString(string: "Cannot be empty",
                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])


        if userName.isEmpty {
            txtUsername.attributedPlaceholder = redPlaceholder
            if address.isEmpty{
                txtAddress.attributedPlaceholder = redPlaceholder
                if email.isEmpty {
                    txtEmail.attributedPlaceholder = redPlaceholder
                    if phoneNum.isEmpty{
                        txtPhoneNum.attributedPlaceholder = redPlaceholder
                    }
                }
            }
        }else{
            let secondView = storyboard?.instantiateViewController(identifier: "SecondView") as! SecondViewController
            secondView.modalPresentationStyle = .fullScreen
            present(secondView, animated: true, completion: nil)
            self.delegate = secondView
            self.delegate?.didSend(userName: userName, email: email, address: address, phoneNum: phoneNum)
            self.delegate?.showImg(img: image!, color:imageColor! )
        }
    }
}

extension ViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        
            if let nextResponder = textField.superview?.viewWithTag(nextTag)  {
                nextResponder.becomeFirstResponder()
            }else {
                textField.resignFirstResponder()
            }
        return true
    }
}
