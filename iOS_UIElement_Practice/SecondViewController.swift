//
//  SecondViewController.swift
//  iOS_UIElement_Practice
//
//  Created by Nyta on 11/13/20.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var showInput: UITextView!
    
    var userName : String = ""
    var email : String = ""
    var address : String = ""
    var phoneNum : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showInput.isEditable = false
    }
    
    @IBAction func btnBackward(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension SecondViewController: MessageDelegate{
    
    func didSend(userName: String, email: String, address: String, phoneNum: String) {
        self.userName = userName
        self.email = email
        self.address = address
        self.phoneNum = phoneNum
        showInput.text = "Username: \(userName) \nAddress: \(address) \nEmail: \(email) \nPhone Number: \(phoneNum)"
    }
    
    func showImg(img: UIImage, color: UIColor) {
        imgView.image = img
        imgView.tintColor = color
    }
}
